package com.blueblitz.braintrainer;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

/**
 * Created by arthurabreu on 1/18/2017.
 */


public class Splash extends Activity {



    // ===========================================================

    // Fields

    // ===========================================================



    private final int SPLASH_DISPLAY_LENGHT = 3000;    //Runs for 3 seconds



    // ===========================================================

    // "Constructors"

    // ===========================================================



    // Called when the activity is first created.

    @Override

    public void onCreate(Bundle icicle) {

        super.onCreate(icicle);

        setContentView(R.layout.activity_splash);



                /* New Handler to start the Menu-Activity

                 * and close this Splash-Screen after some seconds.*/

        new Handler().postDelayed(new Runnable(){

            @Override

            public void run() {

                                /* Create an Intent that will start the Menu-Activity. */

                Intent mainIntent = new Intent(Splash.this,GoActivity.class); //GoActivity is the starter activity in your app

                Splash.this.startActivity(mainIntent);

                Splash.this.finish();

            }

        }, SPLASH_DISPLAY_LENGHT);

    }

}